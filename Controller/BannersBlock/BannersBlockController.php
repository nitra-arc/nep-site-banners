<?php

namespace Nitra\BannersBundle\Controller\BannersBlock;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class BannersBlockController extends NitraController
{
    /**
     * Блок баннеров
     * @Template("NitraBannersBundle:BannersBlock:BannersBlock.html.twig")
     *
     * @param string  $location Location
     * @param string  $type     Location type
     * @param string  $size     Image filter name
     * @param integer $limit    Limit of banners
     *
     * @return array Template context
     */
    public function bannersblockAction($location, $type, $size, $limit = null)
    {
        $store = $this->getStore();

        if ($type == 'category') {
            $banners = $this->findBannersByCategory($location, $type, $store['id'], $limit);
        } else {
            $banners = $this->getDocumentManager()->createQueryBuilder('NitraBannersBundle:Banner')
                ->field('locationType')->equals($type)
                ->field('locationLink')->equals($location)
                ->field('isActive')->equals(true)
                ->field('stores.id')->equals($store['id'])
                ->sort('sortOrder', 'asc')
                ->limit($limit)
                ->getQuery()->execute();
        }

        return array(
            'banners' => $banners,
            'size'    => $size,
        );
    }

    /**
     * Find banners by category
     *
     * @param string  $categoryId
     * @param string  $type
     * @param string  $storeId
     * @param integer $limit
     *
     * @return \Nitra\BannersBundle\Document\Banner[]
     */
    protected function findBannersByCategory($categoryId, $type, $storeId, $limit)
    {
        // find banners by category
        $banners = $this->getDocumentManager()->createQueryBuilder('NitraBannersBundle:Banner')
            ->field('locationType')->equals($type)
            ->field('locationCategory.id')->equals($categoryId)
            ->field('isActive')->equals(true)
            ->field('stores.id')->equals($storeId)
            ->sort('sortOrder', 'asc')
            ->limit($limit)
            ->getQuery()->execute();

        // if banners found
        return $banners->count()
            // return him
            ? $banners
            // else - find banners for home page
            : $this->getDocumentManager()->createQueryBuilder('NitraBannersBundle:Banner')
                ->field('locationType')->equals('link')
                ->field('locationLink')->equals('/')
                ->field('isActive')->equals(true)
                ->field('stores.id')->equals($storeId)
                ->sort('sortOrder', 'asc')
                ->limit($limit)
                ->getQuery()->execute();
    }
}