# BannersBunddle

## Описание
Данный банлд предназначен для работы (вывода, обработки) с:

* баннерами

## Подключение
Для подключения данного модуля в проект необходимо:

* composer.json:

```json
{
    ...   
    "require": {
        ...
        "e-commerce-site/bannersbundle": "2.0",
        ...
    }
    ...
}
```

* app/AppKernel.php:

```php
<?php

    //...
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\BannersBundle\NitraBannersBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

## Описание функций

* bannersblockAction - вывод блока баннера

### Описание принимающих параметров

* $location - страница
* $size - размер блока

### Пример добавление в шаблоне

```
# vendor/e-commerce-site/storebundle/Nitra/StoreBundle/Resources/views/Home/homePage.html.twig
# ...
        {% render controller("NitraBannersBundle:BannersBlock/BannersBlock:BannersBlock", {'location':'home', 'size':'baner_home'}) %}
# ...
```
